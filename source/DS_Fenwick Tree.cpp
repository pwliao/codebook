struct fenwick
{
	const static int MAXN = 100005;
	int a[MAXN], n;
	fenwick(int _n)
	{
		n = _n;
		memset(a, 0, sizeof(a));
	}
	void add(int i, int x)
	{
		while (i <= n)
		{
			a[i] += x;
			i += i & -i;
		}
	}
	int query(int i)
	{
		int sum = 0;
		while (i > 0)
		{
			sum += a[i];
			i -= i & -i;
		}
		return sum;
	}
};
