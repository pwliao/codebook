#include <cstdio>
#include <cstring>
#include <vector>
using namespace std;
const int MAXV = 50000;
struct two_sat
{
	vector<int> adj[MAXV * 2 + 5];
	vector<int> radj[MAXV * 2 + 5];
	int visit[MAXV * 2 + 5];
	int ans[MAXV * 2 + 5];
	int finish[MAXV * 2 + 5];
	int scc[MAXV * 2 + 5];
	int n, cur;
	void init(int num_var)
	{
		n = num_var;
		for (int i = 0; i <= n * 2; i++)
		{
			adj[i].clear();
			visit[i] = 0;
			ans[i] = 0;
			finish[i] = 0;
			scc[i] = 0;
		}
	}
	void addClause(int A, bool TA, int B, bool TB)
	{
		A = A << 1 | TA;
		B = B << 1 | TB;
		adj[A ^ 1].push_back(B);
		adj[B ^ 1].push_back(A);
		radj[A].push_back(B ^ 1);
		radj[B].push_back(A ^ 1);
	}
	void dfs1(int x)
	{
		visit[x] = 1;
		for (int i = 0; i < (int)adj[x].size(); i++)
			if (!visit[adj[x][i]])
				dfs1(adj[x][i]);
		finish[cur++] = x;
	}
	void dfs2(int x)
	{
		scc[x] = cur;
		visit[x] = 1;
		for (int i = 0; i < (int)radj[x].size(); i++)
			if (!visit[radj[x][i]])
				dfs2(radj[x][i]);
	}
	bool sat()
	{
		cur = 0;
		memset(visit, 0, sizeof(visit));
		for (int i = 0; i < n * 2; i++)
			if (!visit[i]) dfs1(i);
		cur = 0;
		memset(visit, 0, sizeof(visit));
		for (int i = n * 2 - 1; i >= 0; i--)
		{
			int x = finish[i];
			if (!visit[x])
			{
				dfs2(x);
				cur++;
			}
		}
		for (int i = 0; i < 2 * n; i += 2)
			if (scc[i] == scc[i ^ 1]) return false;
		memset(ans, 0, sizeof(ans));
		for (int i = 0; i < n * 2; i++)
		{
			int x = finish[i];
			if (!ans[scc[x]])
			{
				ans[scc[x]] = 1;
				ans[scc[x ^ 1]] = -1;
			}
		}
		return true;
	}
	bool T(int x)
	{
		return ans[scc[x << 1]] == -1;
	}
};
