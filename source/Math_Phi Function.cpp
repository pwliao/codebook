#include <cstdio>
const int N = 100000;
int phi[N];
int main()
{
	for (int i = 1; i < N; i++) phi[i] = i;
	for (int i = 1; i < N; i++)
		for (int j = i + i; j < N; j += i)
			phi[j] -= phi[i];
	return 0;
}
