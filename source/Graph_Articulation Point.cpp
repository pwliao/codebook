#include<cstdio>
#include<vector>
#include<algorithm>

using namespace std;

const int N = 100005;
int dfn[N], low[N], t = 0;
vector<int> g[N], ans;

void dfs(int d)
{
	dfn[d] = low[d] = ++t;
	bool ap = false;
	int child = 0;
	for (auto i : g[d])
	{
		if (dfn[i])
			low[d] = min(dfn[i], low[d]);
		else
		{
			child++;
			dfs(i);
			low[d] = min(low[d], low[i]);
			if (low[i] >= dfn[d]) ap = true;
		}
	}
	if ((d == 0 && child > 1) || (d != 0 && ap))
		ans.push_back(d);
}
int main()
{
	int v, e;
	scanf("%d%d", &v, &e);
	while (e--)
	{
		int s, t;
		scanf("%d%d", &s, &t);
		g[s].push_back(t);
		g[t].push_back(s);
	}
	dfs(0);
	sort(ans.begin(), ans.end());
	for (auto i : ans)
		printf("%d\n", i);
	return 0;
}
