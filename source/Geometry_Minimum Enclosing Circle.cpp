#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
using namespace std;
#define N 305

class Point{
public:
	double x,y;
};

Point p[3];
Point data[N];
bool diameterType;

inline double disquare(Point &p1, Point &p2){
	return (p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y);
}

inline Point findCenter(){
	Point center;
	if(diameterType){
		center.x = (p[0].x+p[1].x)/2;
		center.y = (p[0].y+p[1].y)/2;
		return center;
	}
	double A = p[0].x - p[1].x;
	double B = p[0].y - p[1].y;
	double C = p[0].x - p[2].x;
	double D = p[0].y - p[2].y;
	double E = (A*(p[0].x+p[1].x)+B*(p[0].y+p[1].y))/2;
	double F = (C*(p[0].x+p[2].x)+D*(p[0].y+p[2].y))/2;
	center.x = (D*E-B*F)/(A*D-B*C);
	center.y = (A*F-C*E)/(A*D-B*C);
	return center;
}

inline bool inCircle(int i){
	if(diameterType){
		return disquare(p[0],data[i])+disquare(p[1],data[i]) <= disquare(p[0],p[1]);
	}
	Point center = findCenter();
	return disquare(center,data[i])<=disquare(center,p[0]);
}

void makeCircle(int n,int determine=0){//n should be greater than 1.
	if(determine==3) return;
	if(n==0) return;
	for(int i=0;i+determine<2;i++){
		p[i+determine] = data[i];
	}
	diameterType = 1;
	for(int i=2-determine;i<n;i++){
		if(inCircle(i)) continue;
		if(determine==2) diameterType = 0;
		p[determine] = data[i];
		makeCircle(i,determine+1);
	}
}

void solve(){
	int n;
	scanf("%d",&n);
	for(int i=0;i<n;i++){
		scanf("%lf%lf",&data[i].x,&data[i].y);
	}
	if(n==1){
		puts("0.00");
		return;
	}
	makeCircle(n);
	Point center = findCenter();
	double r = sqrt(4*disquare(p[0],center));
	printf("%.2f\n",r);
}

int main()
{
	solve();
	return 0;
}
