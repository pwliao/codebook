#include <cstdio>
int v, e, dis[100][100];
const int INF = 0x3fffffff;
bool negcycle()
{
	for (int i = 0; i < v; i++)
		if (dis[i][i] < 0)
			return true;
	return false;
}
int main()
{
	int s, t, d;
	scanf("%d%d", &v, &e);
	for (int i = 0; i < v; i++)
		for (int j = 0; j < v; j++)
			if (i != j) dis[i][j] = INF;
			else dis[i][j] = 0;
	while (e--)
	{
		scanf("%d%d%d", &s, &t, &d);
		dis[s][t] = d;
	}
	for (int k = 0; k < v; k++)
		for (int i = 0; i < v; i++)
			for (int j = 0; j < v; j++)
				if (/*dis[i][k]!=INF && dis[k][j]!=INF && */dis[i][k] + dis[k][j] < dis[i][j])
					dis[i][j] = dis[i][k] + dis[k][j];
	if (negcycle())
		puts("NEGATIVE CYCLE");
	else
	{
		for (int i = 0; i < v; i++)
		{
			for (int j = 0; j < v; j++)
			{
				if (j) putchar(' ');
				if (dis[i][j] == INF) printf("INF");
				else printf("%d", dis[i][j]);
			}
			putchar('\n');
		}
	}
	return 0;
}
