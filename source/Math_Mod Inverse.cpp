// a*x + b*y = g = gcd(a,b);
void extgcd(int a, int b, int &x, int &y, int &g)
{
	int m[2][3] = {1, 0, a, 0, 1, b};
	while(m[1][2])
	{
		int t = m[0][2] / m[1][2], r;
		for(int i = 0; i < 3; i++)
		{
			r = m[1][i];
			m[1][i] = m[0][i] - t * m[1][i];
			m[0][i] = r;
		}
	}
	g = m[0][2], x = m[0][0], y = m[0][1];
}
int modinverse(int a, int mod)
{
	int x, y, g;
	extgcd(a, mod, x, y, g);
	if (x < 0) x += mod;
	return x;
}
