#include<cstdio>
#include<cstring>
#include<vector>
#include<algorithm>
using namespace std;
const int N = 902;
int n, root, par[N], lca[N][N], f[N], cnt[N];
vector<int> g[N];
bool visited[N];
int Find(int x)
{
	if (f[x] == x) return x;
	return f[x] = Find(f[x]);
}
void Union(int x, int y)
{
	int fx = Find(x), fy = Find(y);
	f[fx] = fy;
}
void getroot(int a)
{
	if (par[a] == -1)
		root = a;
	else
		getroot(par[a]);
}
void dfs(int d)
{
	visited[d] = true;
	for (int i = 1; i <= n; i++)
		if (visited[i])
			lca[d][i] = lca[i][d] = Find(i);
	for (int i = 0; i < (int)g[d].size(); i++)
		if (!visited[g[d][i]])
			dfs(g[d][i]);
	Union(d, par[d]);
}
int main()
{
	while (scanf("%d", &n) != EOF)
	{
		memset(par, -1, sizeof(par));
		memset(visited, 0, sizeof(visited));
		memset(cnt, 0, sizeof(cnt));
		for (int i = 0; i <= n; i++)
		{
			f[i] = i;
			g[i].clear();
		}
		for (int i = 0; i < n; i++)
		{
			int a, b, c;
			scanf("%d:(%d)", &a, &b);
			for (int j = 0; j < b; j++)
			{
				scanf("%d", &c);
				g[a].push_back(c);
				par[c] = a;
			}
		}
		getroot(n);
		dfs(root);
		int m;
		scanf("%d", &m);
		while (m--)
		{
			int a, b;
			scanf(" (%d%d) ", &a, &b);
			cnt[lca[a][b]]++;
		}
		for (int i = 1; i <= n; i++)
			if (cnt[i])
				printf("%d:%d\n", i, cnt[i]);
	}
	return 0;
}
