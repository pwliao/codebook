#include <cstdio>
#include <queue>
#include <vector>
#include <algorithm>
using namespace std;
struct node
{
	int n, dis;
	node() {}
	node(int _n, int _d): n(_n), dis(_d) {}
	bool operator<(const node &rhs) const
	{
		return dis > rhs.dis;
	}
};
const int MAXN = 100005;
const int INF = 0x3fffffff;
int v, e, r;
vector<node> g[MAXN];
int dis[MAXN];
bool vis[MAXN];
int main()
{
	scanf("%d%d%d", &v, &e, &r);
	for (int i = 0; i < e; i++)
	{
		int s, t, d;
		scanf("%d%d%d", &s, &t, &d);
		g[s].push_back(node(t, d));
	}
	fill(dis, dis + v, INF);
	fill(vis, vis + v, false);
	dis[r] = 0;
	priority_queue<node> pq;
	pq.push(node(r, 0));
	while (!pq.empty())
	{
		node t = pq.top();
		pq.pop();
		if (vis[t.n]) continue;
		vis[t.n] = true;
		for (auto &i : g[t.n])
		{
			if (!vis[i.n] && dis[t.n] + i.dis < dis[i.n])
			{
				dis[i.n] = dis[t.n] + i.dis;
				pq.push(node(i.n, dis[i.n]));
			}
		}
	}
	for (int i = 0; i < v; i++)
	{
		if (dis[i] == INF)
			puts("INF");
		else
			printf("%d\n", dis[i]);
	}
	return 0;
}
