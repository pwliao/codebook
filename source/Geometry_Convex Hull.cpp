#include<cstdio>
#include<stack>
#include<vector>
#include<algorithm>

using namespace std;

struct Point
{
	int index;
	double x;
	double y;
};

Point p[10000];
int ans_up[10000];
int ans_down[10000];
char s[1000];

bool cpr(Point a, Point b)
{
	if(a.x<b.x) return 1;
	if(a.x==b.x&&a.y<b.y) return 1;
	return 0;
}

bool ccw(Point a, Point b, Point c)
{
	return (b.x-a.x)*(c.y-b.y)>=(c.x-b.x)*(b.y-a.y);
}

int main()
{
	int i, n, index_up, index_down;
	scanf("%d", &n);
	for(i=0;i<n;i++){
		scanf("%lf %lf", &p[i].x, &p[i].y);
		p[i].index=i;
	}
	sort(p, p+n, cpr);
	ans_down[0]=0;
	ans_down[1]=1;
	index_down=2;
	for(i=2;i<n;i++){
		while(index_down>1&&!ccw(p[ans_down[index_down-2]], p[ans_down[index_down-1]], p[i])) index_down--;
		ans_down[index_down]=i;
		index_down++;
	}
	ans_up[0]=n-1;
	ans_up[1]=n-2;
	index_up=2;
	for(i=n-3;i>=0;i--){
		while(index_up>1&&!ccw(p[ans_up[index_up-2]], p[ans_up[index_up-1]], p[i])) index_up--;
		ans_up[index_up]=i;
		index_up++;
	}

	for(i=0;i<index_down-1;i++) printf("%d %f %f\n", p[ans_down[i]].index, p[ans_down[i]].x, p[ans_down[i]].y);
	for(i=0;i<index_up-1;i++) printf("%d %f %f\n", p[ans_up[i]].index, p[ans_up[i]].x, p[ans_up[i]].y);
}
