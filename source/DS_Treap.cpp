#include <cstdio>
#include <cstdlib>
#include <ctime>
struct Treap
{
	int key, pri, size;
	Treap *l, *r;
	Treap() {}
	Treap(int _key)
		: key(_key), pri(rand()), size(1), l(NULL), r(NULL) {}
};
int size(Treap *t)
{
	return t ? t->size : 0;
}
void pull(Treap *t)
{
	t->size = size(t->l) + size(t->r) + 1;
}
Treap *merge(Treap *a, Treap *b)
{
	if (!a || !b) return a ? a : b;
	if (a->pri > b->pri)
	{
		a->r = merge(a->r, b);
		pull(a);
		return a;
	}
	else
	{
		b->l = merge(a, b->l);
		pull(b);
		return b;
	}
}

void split(Treap *t, int k, Treap *&a, Treap *&b)
{
	if (!t) a = b = NULL;
	else if (t->key <= k)
	{
		a = t;
		split(t->r, k, a->r, b);
		pull(a);
	}
	else
	{
		b = t;
		split(t->l, k, a, b->l);
		pull(b);
	}
}
void splitSize(Treap *t, int k, Treap *&a, Treap *&b)
{
	if (!t) a = b = NULL;
	else if (size(t->l) < k)
	{
		a = t;
		splitSize(t->r, k - size(t->l) - 1, a->r, b);
		pull(a);
	}
	else
	{
		b = t;
		splitSize(t->l, k, a, b->l);
		pull(b);
	}
}
Treap *insert(Treap *t, int k)
{
	Treap *a, *b;
	split(t, k, a, b);
	return merge(merge(a, new Treap(k)), b);
}
int kth(Treap *t, int k)
{
	if (k <= size(t->l)) return kth(t->l, k);
	else if (k == size(t->l) + 1) return t->key;
	else return kth(t->r, k - size(t->l) - 1);
}
Treap *remove(Treap *t, int k)
{
	if (t->key == k)
	{
		Treap *tl = t->l, *tr = t->r;
		delete t;
		return merge(tl, tr);
	}
	else if (k < t->key)
		t->l = remove(t->l, k);
	else
		t->r = remove(t->r, k);
	pull(t);
	return t;
}
int main()
{
	return 0;
}
