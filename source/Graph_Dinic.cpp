#include <cstdio>
#include <cstring>
#include <vector>
#include <queue>
#include <algorithm>

using namespace std;

template<class T>
struct Dinic
{
	static const int MAXV = 1005;
	static const T INF = 1e15;
	struct edge
	{
		int to; T f; int rev;
	};
	int source, sink, dis[MAXV];
	int now[MAXV];
	vector<edge> E[MAXV];
	void init(int _s, int _t, int n)
	{
		source = _s, sink = _t;
		for (int i = 0; i < n; i++)
			E[i].clear();
	}
	void addEdge(int from, int to, T ff, T tf)
	{
		E[from].push_back({to, ff, (int)E[to].size()});
		E[to].push_back({from, tf, (int)E[from].size() - 1});
	}
	bool bfs()
	{
		memset(dis, -1, sizeof(dis));
		queue<int> q;
		q.push(source);
		dis[source] = 0;
		while (!q.empty())
		{
			int t = q.front();
			q.pop();
			for (const auto &i : E[t])
			{
				if (i.f && dis[i.to] == -1)
				{
					q.push(i.to);
					dis[i.to] = dis[t] + 1;
				}
			}
		}
		return dis[sink] != -1;
	}
	T dfs(int x, T minf)
	{
		if (x == sink) return minf;
		for (int &k = now[x]; k < (int)E[x].size(); k++)
		{
			edge &i = E[x][now[x]];
			if (i.f && dis[i.to] == dis[x] + 1)
			{
				T ret = dfs(i.to, min(minf, i.f));
				if (ret)
				{
					i.f -= ret;
					E[i.to][i.rev].f += ret;
					return ret;
				}
			}
		}
		return 0;
	}
	T maxFlow()
	{
		T f, max_flow = 0;
		while (bfs())
		{
			memset(now, 0, sizeof(now));
			while (f = dfs(source, INF))
				max_flow += f;
		}
		return max_flow;
	}
};
