#include <cstdio>
#include <cmath>
#include <vector>
using namespace std;
vector<long long> T[20], X[20];
void push(int h, int k)
{
	T[h][k] += (X[h][k] << h);
	if(h)
	{
		X[h - 1][k << 1] += X[h][k];
		X[h - 1][k << 1 | 1] += X[h][k];
	}
	X[h][k] = 0;
}
void pull(int h, int k)
{
	long long sl = T[h - 1][k << 1] + (X[h - 1][k << 1] << h - 1);
	long long sr = T[h - 1][k << 1 | 1] + (X[h - 1][k << 1 | 1] << h - 1);
	T[h][k] = sl + sr;
}
long long query(int h, int k, int i, int j)
{
	push(h, k);
	int m = (k << h) + (1 << h - 1);
	if(i == (k << h) && j == (k + 1 << h) - 1) return T[h][k];
	if(j < m) return query(h - 1, k << 1, i, j);
	if(i >= m) return query(h - 1, k << 1 | 1, i, j);
	return query(h - 1, k << 1, i, m - 1) + query(h - 1, k << 1 | 1, m, j);
}
void update(int h, int k, int i, int j, int x)
{
	push(h, k);
	int m = (k << h) + (1 << h - 1);
	if(i == (k << h) && j == (k + 1 << h) - 1)
	{
		X[h][k] = x;
		return;
	}
	if(j < m)  update(h - 1, k << 1, i, j, x);
	else if(i >= m)  update(h - 1, k << 1 | 1, i, j, x);
	else
	{
		update(h - 1, k << 1, i, m - 1, x);
		update(h - 1, k << 1 | 1, m, j, x);
	}
	pull(h, k);
}
int main()
{
	int n, q;
	while(~scanf("%d%d", &n, &q))
	{
		int d = ceil(log2(n));
		for(int i = 0; i <= d; i++)
		{
			T[i].clear();
			T[i].resize(1 << (d - i), 0);
			X[i].clear();
			X[i].resize(1 << (d - i), 0);
		}
		for(int i = 0; i < n; i++)
			scanf("%I64d", &T[0][i]);
		for(int i = 1; i <= d; i++)
			for(int j = 0; j < T[i].size(); j++)
				T[i][j] = T[i - 1][j << 1] + T[i - 1][j << 1 | 1];
		while(q--)
		{
			char c;
			int i, j, k;
			scanf(" %c%d%d", &c, &i, &j);
			if(c == 'Q')
				printf("%I64d\n", query(d, 0, i - 1, j - 1));
			else
			{
				scanf("%d", &k);
				update(d, 0, i - 1, j - 1, k);
			}
		}
	}
	return 0;
}
