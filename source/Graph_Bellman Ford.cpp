// C++11
#include <cstdio>
const int INF = 2147483647;
int dis[1000];
struct node
{
	int s, t, d;
} edge[2000];
int main()
{
	int v, e, r;
	scanf("%d%d%d", &v, &e, &r);
	for (int i = 0; i < v; i++)
		dis[i] = INF;
	dis[r] = 0;
	for (int i = 0; i < e; i++)
	{
		int s, t, d;
		scanf("%d%d%d", &s, &t, &d);
		edge[i] = node{s, t, d};
	}
	bool negcyc = false;
	for (int i = 0; i < v; i++)
	{
		bool update = false;
		for (int j = 0; j < e; j++)
		{
			node &t = edge[j];
			if (dis[t.s] != INF && (dis[t.s] + t.d) < dis[t.t])
			{
				dis[t.t] = dis[t.s] + t.d;
				update = true;
			}
		}
		if (!update) break;
		if (i == v - 1) negcyc = true;
	}
	if (negcyc)
		puts("NEGATIVE CYCLE");
	else
		for (int i = 0; i < v; i++)
			if (dis[i] == INF)
				puts("INF");
			else
				printf("%d\n", dis[i]);
	return 0;
}
