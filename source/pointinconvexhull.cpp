#include<cstdio>
#include<map>
#include<algorithm>

using namespace std;

class point
{
public:
    int index;
    long long x;
    long long y;
    bool operator<(const point P)const
    {
        if(x<P.x) return 1;
        if(x==P.x&&y<P.y) return 1;
        return 0;
    }
};

point L[10005];
point S[50005];
int ans_up[10005];
int ans_down[10005];

/*bool cpr(point a, point b)
{
    if(a.x<b.x) return 1;
    if(a.x==b.x&&a.y<b.y) return 1;
    return 0;
}*/

bool ccw(point a, point b, point c)
{
    return (b.x-a.x)*(c.y-b.y)>=(c.x-b.x)*(b.y-a.y);
}

int large, small;
map<point, bool> up_tubo;
map<point, bool> down_tubo;
map<point, bool>::iterator it;

bool debugging=0;

int main()
{
    int index_down, index_up;
    scanf("%d", &large);
    for(int i=0;i<large;i++){
        scanf("%I64d %I64d", &L[i].x, &L[i].y);
        L[i].index=i;
    }
    sort(L, L+large);
    ans_down[0]=0;
    ans_down[1]=1;
    index_down=2;
    for(int i=2;i<large;i++){
        while(index_down>1&&!ccw(L[ans_down[index_down-2]], L[ans_down[index_down-1]], L[i])) index_down--;
        ans_down[index_down]=i;
        index_down++;
    }
    ans_up[0]=large-1;
    ans_up[1]=large-2;
    index_up=2;
    for(int i=large-3;i>=0;i--){
        while(index_up>1&&!ccw(L[ans_up[index_up-2]], L[ans_up[index_up-1]], L[i])) index_up--;
        ans_up[index_up]=i;
        index_up++;
    }
    down_tubo.clear();
    down_tubo[L[ans_up[0]]]=1;
    for(int i=0;i<index_down-1;i++){
        down_tubo[L[ans_down[i]]]=1;
    }
    up_tubo.clear();
    up_tubo[L[ans_down[0]]]=1;
    for(int i=0;i<index_up-1;i++){
        up_tubo[L[ans_up[i]]]=1;
    }
    if(debugging){
        puts("up");
        for(it=up_tubo.begin();it!=up_tubo.end();it++){
            printf("%I64d %I64d\n", it->first.x, it->first.y);
        }
        puts("down");
        for(it=down_tubo.begin();it!=down_tubo.end();it++){
            printf("%I64d %I64d\n", it->first.x, it->first.y);
        }
    }
    scanf("%d", &small);
    int cnt=0;
    point p1, p2;
    for(int i=0;i<small;i++){
        scanf("%I64d %I64d", &S[i].x, &S[i].y);
        if(up_tubo.find(S[i])!=up_tubo.end()||down_tubo.find(S[i])!=down_tubo.end()){
            cnt++;
            continue;
        }
        it=up_tubo.upper_bound(S[i]);
        if(it==up_tubo.begin()||it==up_tubo.end()) continue;
        p1=it->first;
        it--;
        p2=it->first;
        if((p1.x-S[i].x)*p2.y+(S[i].x-p2.x)*p1.y<S[i].y*(p1.x-p2.x)) continue;
        it=down_tubo.upper_bound(S[i]);
        if(it==down_tubo.begin()||it==down_tubo.end()) continue;
         p1=it->first;
        it--;
        p2=it->first;
        if((p1.x-S[i].x)*p2.y+(S[i].x-p2.x)*p1.y<=S[i].y*(p1.x-p2.x)){
            cnt++;
            if(debugging) puts("yes");
        }
    }
    printf("%d\n", cnt);
    return 0;
}
