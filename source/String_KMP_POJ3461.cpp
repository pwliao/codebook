#include <cstdio>
#include <cstring>
char p[10005], t[1000005];
int fail[10005] = { -1, 0};
int main()
{
	int T;
	scanf("%d", &T);
	while (T--)
	{
		scanf("%s%s", p, t);
		int l = strlen(p), cnt = 0;
		for (int i = 2, state = fail[1]; i <= l; fail[i++] = ++state)
			while (p[i - 1] != p[state] && ~state) state = fail[state];
		for (int i = 0, state = 0; t[i]; i++)
		{
			while (t[i] != p[state] && ~state) state = fail[state];
			if (++state == l) cnt++;
		}
		printf("%d\n", cnt);
	}
	return 0;
}
