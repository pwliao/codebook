#include <cstdio>
#include <cmath>
#include <algorithm>
using namespace std;
double matrix[3][4] = {{1, 2, 1, 8}, {2, 2, 3, 15}, {1, 3, 2, 13}};
const double EPS = 1e-6;
void gaussian(int n, int m)
{
	for (int i = 0; i < n; i++)
	{
		if (abs(matrix[i][i]) < EPS)
			for (int j = i + 1; j < n; j++)
				if (matrix[j][i] > EPS)
					for (int k = 0; k < m; k++)
						swap(matrix[i][k], matrix[j][k]);
		for (int j = 0; j < n; j++)
		{
			if (i == j) continue;
			double r = matrix[j][i] / matrix[i][i];
			for (int k = 0; k < m; k++)
				matrix[j][k] -= r * matrix[i][k];
		}
		double t = matrix[i][i];
		for (int j = i; j < m; j++)
			matrix[i][j] /= t;
	}
}
int main()
{
	gaussian(3, 4);
	for (int i = 0; i < 3; i++, puts(""))
		for (int j = 0; j < 4; j++)
			printf("%f ", matrix[i][j]);
	return 0;
}
